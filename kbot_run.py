import kbot_agent
import sys

# system mode (0=simple path (straightline), 1=random walk)
mode = int(sys.argv[1])
obs_dist = int(sys.argv[2])

#sys arg for obs dist => distance to avoid obstacle
agent = kbot_agent.Agent(mode, obs_dist)

try:
	agent.run()
except KeyboardInterrupt:
	print("kill the bot...")
	agent.stop()
finally:
	agent.cleanup()
