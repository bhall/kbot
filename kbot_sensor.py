import time

class Sensor:
	def __init__(self, gpio, trig, echo, obs_dist):
		#============GPIO INSTANCE
		self.GPIO = gpio


		#============VARS FOR CALCULATIONS
		self.pulse_time = 0.0
		self.pulse_start = 0.0
		self.pulse_end = 0.0
		self.distance = 0.0
		self.avoid_obs = obs_dist

		#constants
		self.SPEED_OF_SOUND = 17150 # 34300 / 2 cm/s (send and return pulse)

		# pins for sensor I/O
		self.TRIG = trig
		self.ECHO = echo

		# set pins for output
		self.GPIO.setup(self.TRIG, self.GPIO.OUT)
		self.GPIO.setup(self.ECHO, self.GPIO.IN)

		# settle sensor
		self.GPIO.output(self.TRIG, False)
		print("Waiting For Sensor To Settle")
		time.sleep(2)

	def find_distance(self):
		# 10us pulse to trigger sensor program
		self.GPIO.output(self.TRIG, True)
		time.sleep(0.00001)

		# make sure TRIG is off
		self.GPIO.output(self.TRIG, False)

		# wait for event trigger
		while self.GPIO.input(self.ECHO) == 0:
				self.pulse_start = time.time()

		# wait for event trigger
		while self.GPIO.input(self.ECHO) == 1:
				self.pulse_end = time.time()

		# calculate time for pulse
		self.pulse_time = self.pulse_end - self.pulse_start
		# calculate distance from velocity and time
		self.distance = self.SPEED_OF_SOUND * self.pulse_time

		#display the result
		#print("Distance: ", self.distance)

		time.sleep(0.05)
		return self.distance

	def obstacle_found(self):
		if self.distance <= self.avoid_obs:
			return True
		else:
			return False
