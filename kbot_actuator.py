import time

class DualMotorActuator:
	def __init__(self, gpio, a_in_1, a_in_2, b_in_1, b_in_2, slp):
		#pin numbers
		self.a1 = a_in_1
		self.a2 = a_in_2
		self.b1 = b_in_1
		self.b2 = b_in_2
		self.slp = slp

		#gpio pin object
		self.GPIO = gpio
		
		#configure pins
		self.GPIO.setup(self.a1, self.GPIO.OUT)
		self.GPIO.setup(self.a2, self.GPIO.OUT)
		self.GPIO.setup(self.b1, self.GPIO.OUT)
		self.GPIO.setup(self.b2, self.GPIO.OUT)
		self.GPIO.setup(self.slp, self.GPIO.OUT)

		#slp pin set to logic high
		self.GPIO.output(self.slp, True)

	def stop(self):
		"""
		stop all motion
		"""
		self.GPIO.output(self.a1, False)
		self.GPIO.output(self.a2, False)
		self.GPIO.output(self.b1, False)
		self.GPIO.output(self.b2, False)

	def forward(self):
		#print("forward")
		self.GPIO.output(self.a2, True)
		self.GPIO.output(self.b1, True)

	def forward_right(self):
		#print("forward right")
		self.GPIO.output(self.a2, True)
		self.GPIO.output(self.b1, False)

	def forward_left(self):
		#print("forward left")
		self.GPIO.output(self.a2, False)
		self.GPIO.output(self.b1, True)

	def backward(self):
		#print("backward")
		self.GPIO.output(self.a1, True)
		self.GPIO.output(self.b2, True)

	def backward_right(self):
		#print("backward right")
		self.GPIO.output(self.a1, True)
		self.GPIO.output(self.b2, False)

	def backward_left(self):
		#print("backward left")
		self.GPIO.output(self.a1, False)
		self.GPIO.output(self.b2, True)

	def test(self, test_time):
		"""
		Test the pins and see which ones activate and deactivate motors
		test_time is seconds to run test
		"""
		#pins for A1 input
		print("Testing A1")
		self.GPIO.output(self.a1, True)
		time.sleep(test_time)
		self.GPIO.output(self.a1, False)

		#pins for A2 input
		print("Testing A2")
		self.GPIO.output(self.a2, True)
		time.sleep(test_time)
		self.GPIO.output(self.a2, False)

		#pins for B1 input
		print("Testing B1")
		self.GPIO.output(self.b1, True)
		time.sleep(test_time)
		self.GPIO.output(self.b1, False)

		#pins for B2 input
		print("Testing B2")
		self.GPIO.output(self.b2, True)
		time.sleep(test_time)
		self.GPIO.output(self.b2, False)

		#test foward
		print("foward")
		self.forward()
		time.sleep(test_time)
		self.stop()

		#test foward left
		print("foward left")
		self.forward_left()
		time.sleep(test_time)
		self.stop()

		#test foward right
		print("foward right")
		self.forward_right()
		time.sleep(test_time)
		self.stop()

		#test backward
		print("backward")
		self.backward()
		time.sleep(test_time)
		self.stop()

class SingleMotorActuator:
	def __init__(self, gpio, a_in_1, a_in_2, b_in_1, b_in_2, slp):
		#pin numbers
		self.a1 = a_in_1
		self.a2 = a_in_2
		self.b1 = b_in_1
		self.b2 = b_in_2
		self.slp = slp

		#gpio pin object
		self.GPIO = gpio
		
		#configure pins
		self.GPIO.setup(self.a1, self.GPIO.OUT)
		self.GPIO.setup(self.a2, self.GPIO.OUT)
		self.GPIO.setup(self.b1, self.GPIO.OUT)
		self.GPIO.setup(self.b2, self.GPIO.OUT)
		self.GPIO.setup(self.slp, self.GPIO.OUT)

		#slp pin set to logic high
		self.GPIO.output(self.slp, True)

	def stop(self):
		"""
		stop all motion
		"""
		self.GPIO.output(self.a1, False)
		self.GPIO.output(self.a2, False)
		self.GPIO.output(self.b1, False)
		self.GPIO.output(self.b2, False)

	def forward(self):
		# turn backwheels forward
		print("forward")
		GPIO.output(self.a1, True)
		GPIO.output(self.a2, False)
		GPIO.output(self.b1, False)
		GPIO.output(self.b2, False)

	def forward_right(self):
		# turn front wheels right
		print("forward right")
		GPIO.output(self.a1, True)
		GPIO.output(self.a2, False)
		GPIO.output(self.b1, False)
		GPIO.output(self.b2, True)

	def forward_left(self):
		# turn front wheels left
		print("forward left")
		GPIO.output(self.a1, True)
		GPIO.output(self.a2, False)
		GPIO.output(self.b1, True)
		GPIO.output(self.b2, False)

	def backward(self):
		# turn back wheels reverse
		print("reverse")
		GPIO.output(self.a1, False)
		GPIO.output(self.a2, True)
		GPIO.output(self.b1, False)
		GPIO.output(self.b2, False)

	def backward_right(self):
		# turn front wheels right
		print("reverse right")
		GPIO.output(self.a1, False)
		GPIO.output(self.a2, True)
		GPIO.output(self.b1, False)
		GPIO.output(self.b2, True)
		
	def backward_left(self):
		# turn front wheels left
		print("reverse left")
		GPIO.output(self.a1, False)
		GPIO.output(self.a2, True)
		GPIO.output(self.b1, True)
		GPIO.output(self.b2, False)

	def test(self, test_time):
		"""
		Test the pins and see which ones activate and deactivate motors
		test_time is seconds to run test
		"""
		#pins for A1 input
		print("Testing A1 (forward)")
		self.GPIO.output(self.a1, True)
		time.sleep(test_time)
		self.GPIO.output(self.a1, False)

		#pins for A2 input
		print("Testing A2 (reverse)")
		self.GPIO.output(self.a2, True)
		time.sleep(test_time)
		self.GPIO.output(self.a2, False)

		#pins for B1 input
		print("Testing B1 (left turn)")
		self.GPIO.output(self.b1, True)
		time.sleep(test_time)
		self.GPIO.output(self.b1, False)

		#pins for B2 input
		print("Testing B2 (right turn)")
		self.GPIO.output(self.b2, True)
		time.sleep(test_time)
		self.GPIO.output(self.b2, False)

		#test foward
		print("foward")
		self.forward()
		time.sleep(test_time)
		self.stop()

		#test foward left
		print("foward left")
		self.forward_left()
		time.sleep(test_time)
		self.stop()

		#test foward right
		print("foward right")
		self.forward_right()
		time.sleep(test_time)
		self.stop()

		#test backward
		print("backward")
		self.backward()
		time.sleep(test_time)
		self.stop()
		
		#test backward left
		print("backward left")
		self.backward_left()
		time.sleep(test_time)
		self.stop()
		
		#test backward
		print("backward right")
		self.backward_right()
		time.sleep(test_time)
		self.stop()
