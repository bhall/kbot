from picamera import PiCamera
import time

class BotCamera:
	def __init__(self, img_dir):
		self.camera = PiCamera()
		self.camera.rotation = 180
		self.img_dir = img_dir
		
		
	def start_camera(self):
		self.camera.start_preview()
		
	def capture_image(self):
		self.camera.capture(self.img_dir + time.time())
		
	def stop_camera(self):
		self.camera.stop_preview())
	
