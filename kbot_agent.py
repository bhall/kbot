import RPi.GPIO as GPIO
import random
import time

#kbot library
import kbot_sensor
import kbot_actuator

class Agent:
	def __init__(self, mode, obs_dist):
		#============RESET PINS
		GPIO.setwarnings(False)
		GPIO.cleanup()

		#============SET UP GPIO
		# set the mode for board numbering scheme
		GPIO.setmode(GPIO.BCM)
		
		#============PIN CLEANUP
		self.clean_list = []

		#============PINS FOR SENSOR
		trig = 24
		echo = 18
		self.clean_list.append(trig)
		self.clean_list.append(echo)
		
		#============INIT SENSOR
		#obs distance should be distance to avoid obstacles
		self.sensor = kbot_sensor.Sensor(GPIO, trig, echo, obs_dist)

		#============PINS FOR ACTUATOR
		a1 = 23 # 
		a2 = 22 # orange - BLK
		b1 = 17 # white - GRN
		b2 = 27 # grey - BLUEs,
		slp = 25
		self.clean_list.append(a1)
		self.clean_list.append(a2)
		self.clean_list.append(b1)
		self.clean_list.append(b2)
		self.clean_list.append(slp)

		#============INIT ACTUATOR
		self.actuator = kbot_actuator.SingleMotorActuator(GPIO, a1, a1, b1, b2, slp)
		#movment mode
		self.mode = mode

		#============POWER SWITCH
		self.switch = False
		self.switch_pin_read = 16
		self.switch_pin_pwr = 26
		self.clean_list.append(self.switch_pin_read)
		self.clean_list.append(self.switch_pin_pwr)
		
		GPIO.setup(self.switch_pin_read, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
		GPIO.setup(self.switch_pin_pwr, GPIO.OUT)
		#will read from pin read, will be True if switch is on
		GPIO.output(self.switch_pin_pwr, GPIO.HIGH)


	def run(self):
		#wait for power to come on
		while self.switch == False:
			self.switch = self.pwr_switch()

		print("powering on...")

		while self.switch == True:
			self.sensor.find_distance()
			while self.sensor.obstacle_found() != True and self.switch == True:
				#print("moving agent")
				self.move(self.mode) # move random walk, 0.3s each move
				self.sensor.find_distance()
				self.switch = self.pwr_switch()

			#print("Obstacle found!")
			self.stop()
			while self.sensor.obstacle_found() == True and self.switch == True:
				#print("escaping obstacle")
				self.escape_obs()
				self.sensor.find_distance()
				self.switch = self.pwr_switch()

			self.stop()

			#if power is switched off -> stop program
			self.switch = self.pwr_switch()

	def pwr_switch(self):
		if GPIO.input(self.switch_pin_read) == 1:
			return True
		else:
			return False


	def move(self, mode):
		if mode == 0:
			#Simple Path
			self.actuator.forward()
		elif mode == 1:
			#Random Walk
			run_time = random.uniform(0.1, 0.5)
			switch = random.randint(0, 2)

			if switch == 0:
				self.actuator.forward()
			elif switch == 1:
				self.actuator.forward_right()
			else:
				self.actuator.forward_left()

			#wait some seconds
			time.sleep(run_time)
		else:
			#Undefined mode
			print("undefined mode... stoping agent")
			self.actuator.stop()

	def stop(self):
		self.actuator.stop()

	def escape_obs(self):
		switch = random.randint(0, 1)

		#wating one sec allows ~90deg turn away from obstacle
		run_time = 1

		if switch == 0:
			self.actuator.backward_left()
		else:
			self.actuator.backward_right()

		#wait for agent to move away from obstacle
		time.sleep(run_time)

	def test(self, test_timer):
		self.actuator.test(test_timer)
		self.sensor.find_distance()

	def test_timer(self, timer, time_a, time_b):
		time = time_b - time_a

		print("time: ", time)

		if time >= timer:
			return True
		else:
			return False

	def cleanup(self):
		for e in self.clean_list:
			GPIO.output(e, GPIO.LOW)
			
		GPIO.cleanup()
